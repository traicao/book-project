<?php
/* @var $this BooksController */
/* @var $model Books */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'books-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'book_name'); ?>
		<?php echo $form->textField($model,'book_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'book_name'); ?>
	</div>

	<div class="row">
        <?php echo CHtml::activeFileField($model, 'book_image'); ?>
<!--		--><?php //echo $form->labelEx($model,'book_image'); ?>
<!--		--><?php //echo $form->textField($model,'book_image',array('size'=>60,'maxlength'=>128)); ?>
<!--		--><?php //echo $form->error($model,'book_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'published_year'); ?>
		<?php echo $form->textField($model,'published_year',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'published_year'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'book_status'); ?>
		<?php echo $form->textField($model,'book_status',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'book_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->